import logo from './logo.svg';
import './App.css';

function App() {

  //une variable let peut être modifié
  let name = "Nelly"
  //une variable const ne peut plus être modifié après sa déclaration
  const Name = "Nelly"

  //pour éviter de faire des concaténations avec les + javascript offre la possibilité avec ``
  let text = `Bonjour  ${name} ${Name}`;

  //déclaration d'objet
  let personne = {
    firstname: "Nelly",
    lastname: "Niamson",
    gender: "F",
    age: 12
  }

  //affectation de nouvelle à une propriété d'un objet
  personne.age = 13;

  //ajout d'une nouvelle propriété dans un objet
  personne['ville'] = "Abidjan";

  //afficage des données d'un objet
  text = `Bonjour je m'appelle ${personne['firstname']} ${personne.lastname}
   je suis de sexe ${personne.gender} et j'ai ${personne.age} ans j'habite à ${personne.ville}`

  
   //declaration de tableau
  let fruits = ["Orange", "Mangue", "Banane", "Orange mur", "Mangue mur", "Banane mur"]

  //parcourt stattique de tableau
  text = `fruit position 0: ${fruits[0]}, fruit position 2: ${fruits[2]}`;

  // parcourt dynamique de tableau
  /* 
    parcourt avec une boucle for in.
    une boucle for in cicle directement les indexs des elements du tableau
  */

  text = "";
  for (const key in fruits) {
    text += `tableau index ${key} est ${fruits[key]},`;
  }

  /* 
    parcourt avec une boucle for of.
    une boucle for of cicle directement les elements du tableau
  */

  text = "";
  for (const value of fruits) {
    text += `${value},`
  }

  return text;
}

export default App;
